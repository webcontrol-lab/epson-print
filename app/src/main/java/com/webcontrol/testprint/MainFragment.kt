package com.webcontrol.testprint

import android.os.Bundle
import android.text.InputFilter
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import com.epson.epos2.Epos2CallbackCode
import com.epson.epos2.Epos2Exception
import com.epson.epos2.printer.Printer
import com.epson.epos2.printer.PrinterStatusInfo
import com.epson.epos2.printer.ReceiveListener
import kotlinx.android.synthetic.main.fragment_main.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class MainFragment : Fragment(), ReceiveListener {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rgConnectionMode.setOnCheckedChangeListener { group, checkedId ->
            if (checkedId == R.id.rbWifi) {
                tilAddress.hint = "IP Address"
            } else if (checkedId == R.id.rbBluetooth) {
                tilAddress.hint = "MAC Address"
                //enableMacAddressInput(txtAddress)
            }
        }

        txtAddress.doOnTextChanged { text, start, count, after ->
            tilAddress.error = null
        }

        btnPrint.setOnClickListener {
            if (!txtAddress.text.toString().isBlank()) {
                print()
            } else {
                tilAddress.error = "Direccion no valida"
            }
        }
    }

    private fun enableMacAddressInput(input: EditText) {
        input.inputType = InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS
        input.setSingleLine()
        val filters = arrayOfNulls<InputFilter>(1)

        filters[0] =
            InputFilter { source, start, end, dest, dstart, dend ->
                if (end > start) {
                    val destTxt = dest.toString()
                    val resultingTxt =
                        destTxt.substring(0, dstart) + source.subSequence(
                            start,
                            end
                        ) + destTxt.substring(dend)
                    when {
                        resultingTxt.matches(Regex("([0-9a-fA-F][0-9a-fA-F]:){0,5}[0-9a-fA-F]")) -> {
                        }
                        resultingTxt.matches(Regex("([0-9a-fA-F][0-9a-fA-F]:){0,4}[0-9a-fA-F][0-9a-fA-F]")) -> {
                            return@InputFilter source.subSequence(start, end).toString() + ":"
                        }
                        resultingTxt.matches(Regex("([0-9a-fA-F][0-9a-fA-F]:){0,5}[0-9a-fA-F][0-9a-fA-F]")) -> {
                        }
                        else -> {
                            return@InputFilter ""
                        }
                    }
                }
                null
            }
        input.filters = filters
    }

    private fun print() {
        var printer: Printer? = null
        try {
            printer = Printer(Printer.TM_P20, Printer.MODEL_ANK, context)
        } catch (e: Epos2Exception) {
            e.printStackTrace()
            Toast.makeText(context, e.message, Toast.LENGTH_SHORT).show()
        }

        printer!!.setReceiveEventListener(this)

        try {
            printer.addTextAlign(Printer.ALIGN_CENTER)
            printer.addText("WebControl Systems Peru\n")
            printer.addFeedLine(1)
            printer.addText("Reserva #2020\n")
            printer.addText("Funcionario: Pablito\n")
            printer.addText("Hotel: Webcontrol\n")
            printer.addText("Pieza: 301\n")
            printer.addFeedLine(2)
            printer.addCut(Printer.CUT_FEED)
        } catch (e: Epos2Exception) {
            e.printStackTrace()
            Toast.makeText(context, e.message, Toast.LENGTH_SHORT).show()
        }

        var connected = false

        try {
            if (rbWifi.isChecked) {
                printer.connect("TCP:${txtAddress.text}", Printer.PARAM_DEFAULT)
            } else {
                printer.connect("BT:${txtAddress.text}", Printer.PARAM_DEFAULT)
            }
            printer.beginTransaction()
            connected = true
        } catch (e: Epos2Exception) {
            e.printStackTrace()
            Toast.makeText(context, "Connection error", Toast.LENGTH_SHORT).show()
        }

        if (connected) {
            val status: PrinterStatusInfo = printer.status
            if (status.connection == 1 && status.online == 1) {
                try {
                    printer.sendData(Printer.PARAM_DEFAULT)
                } catch (e: Epos2Exception) {
                    e.printStackTrace()
                    Toast.makeText(context, e.message, Toast.LENGTH_SHORT).show()
                    finalizePrinterObject(printer)
                }
            } else {
                Toast.makeText(context, "Connection error", Toast.LENGTH_SHORT).show()
                finalizePrinterObject(printer)
            }
        }
    }

    private fun disconnectPrinter(printer: Printer?) {
        if (printer == null) {
            return
        }
        try {
            printer.endTransaction()
            printer.disconnect()
        } catch (e: Epos2Exception) {
            Toast.makeText(context, e.message, Toast.LENGTH_SHORT).show()
        }
    }

    private fun finalizePrinterObject(printer: Printer) {
        printer.clearCommandBuffer()
        printer.setReceiveEventListener(null)
    }

    override fun onPtrReceive(
        printer: Printer?,
        code: Int,
        status: PrinterStatusInfo?,
        jobId: String?
    ) {
        GlobalScope.launch(Dispatchers.Main) {
            if (code == Epos2CallbackCode.CODE_SUCCESS) {
                Toast.makeText(context, "Print success", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(context, "Print error", Toast.LENGTH_SHORT).show()
            }
            disconnectPrinter(printer!!)
        }
    }
}
